PROFILE OF ABDURRAHMAN SHOFY ADIANTO
====================================

# Contact
- no handphone / WA / Line : 0821 4242 5544
- email : abdurrahman@adianto.id
- website : abdurrahman.adianto.id
- based in : Bandung, Indonesia

# Skill
Main technical Skill : Programming

Mastered Programming Language : 
- PHP, CSS, HTML (proficiency 95%)
- JavaScript, Python (proficiency 90%)
- SQL, Bash, C, C++, C#, Pascal (proficiency 60%)

Mastered Softwares : 
- Operating System (Windows & Linux)
- Office (Word, Excel, Powerpoint)
- Axure
- Git
- Apache, MySQL, PostgreSQL
- Pip, Composer
- Geoserver (GIS Software)

Mastered Programming Framework : 
- Yii (PHP, Web)
- Laravel (PHP, Web)
- JQuery
- Unity (C#, Game Development)
- Phaser.JS (JavaScript, Game Development)
- Leaflet.js (Javascript, Map framework)
- PySide (Python Desktop GUI)

# Portofolio
## Notable Project
* CodeJump (2013; https://github.com/azophy/codejump; Programmer; Active)
    Sebuah code-editor berbasis web dengan lisensi open-source, yang dibuat dengan tujuan untuk meningkatkan efektifitas dan produktifitas web-developer, dengan cara menyediakan sebuah code-editor yang ditujukan untuk mengedit sebuah kode langsung dari server.
* SatuPeta Pariwisata Riau (2018; http://pariwisata.riau.go.id/peta , http://edips.riau.go.id/onemap )
    Sebuah program kerjasama antara Dinas Pariwisata dan Ekonomoi Kreatif Provinsi Riau dengan NGO World Resource Institute Indonesia dan Software House TitikKoma Digital. Program ini merupakan proyek crowdsourcing data pariwisata yang bersifat publik dimana masyarakat dapat mengusulkan lokasi wisata yang sebelumnya belum terpublikasikan ke database peta pariwisata Provinsi Riau. Proyek ini melibatkan banyak sekali teknologi open source seperti software GIS Geoserver dan library per-peta-an Leaflet.js, serta Framework Laravel dan database PostgresSQL untuk antarmuka web nya. Situs ini bisa diakses di alamat .
* PertafloSIM ( 2016-sekarang; http://www.pertaflosim.com )
    Sebuah software simulasi perpipaan minyak yang merupakan hasil kerjasama antara Research Technology Center Pertamina dengan Research Consortium Oppinet ITB. Sofware ini merupakan software desktop untuk melakukan simulasi-simulasi terkait sifat-sifat fluida dan sejenisnya. Software ini dikembangkan dengan Python 2.7 berdasarkan framework PySide 1.2.4 . Saya di sini di tim RC-OPPINET ITB sebagai salah satu Programmer utama dan juga mengatur sisi DevOps untuk software ini.

## Project open source / non-profit
* Kelontonk (2011; https://github.com/azophy/kelontong ; Beta Version; Programmer; Discontinued)
    Aplikasi Retail skala kecil menengah berbasis web yang menyasar pasar UMKM. Fokus pada penyediaan solusi yang minus fitur-fitur sekunder demi kemudahan pemakaian di kalangan awam. Merupakan proyek Open Source
* HIMATIKA E-Vote System (2013; https://github.com/azophy/HIMATIKA-e-vote; Programmer)
    Aplikasi sederhana yang digunakan dalam Pemilu Raya Himpunan Mahasiswa Matematika ITB tahun 2012-2013. Saat ini diluncurkan sebagai proyek Open Source dengan harapan dapat menjadi contoh penerapa electronic voting dalam organisasi-organisasi.
* CodeJump (2013; https://github.com/azophy/codejump; Programmer; Active)
    Sebuah code-editor berbasis web dengan lisensi open-source, yang dibuat dengan tujuan untuk meningkatkan efektifitas dan produktifitas web-developer, dengan cara menyediakan sebuah code-editor yang ditujukan untuk mengedit sebuah kode langsung dari server.
* GhostReload (2015; https://github.com/azophy/ghostreload; Programmer; Active)
    Sebuah tool sederhana untuk meningkatkan produktifitas web-developer. Tool ini berguna untuk me-reload suatu halaman browser saat terjadi perubahan pada file/folder (misal, karena men-save file) yang telah ditentukan oleh user.
* Naivechain-PHP (2017; https://github.com/azophy/naivechain-php ; Programmer; Active)
    Proof-of-concept sistem blockchain sederhana menggunakan bahasa pemrograman PHP. Diadaptasi dari artikel sejenis yang lebih populer namun dengan bahasa pemrograman Python
* PHPPixTrack (2019; https://github.com/azophy/PHPPixTrack ; Programmer; Active)
    Tools sederhana untuk melakukan tracking menggunakan pixel tracker. Berguna untuk melakukan tracking bagi user yang sedang mengirimkan email dalam jumlah banyak memanfaatkan fitur mail merge di aplikasi-aplikasi office.

## Project komersial
* GilaLinux.com (2008; Launched; Programmer + Owner; Discontinued)
    Situs e-commerce yang bergerak di bidang penyediaan CD/DVD costumized linux repository dan linux distro.
* IdeaGazer.com (2011; Experimental; Programmer & Owner; Discontinued)
    Aplikasi web yang berfokus pada ide “content oriented presentation slide” dengan antar-muka berbasis web pula.
* JadwalKita.com (2012; Experimental; Programmer & Owner; Discontinued)
    Aplikasi “schedule reminder” yang berfokus pada sharing jadwal kegiatan rutin yang dipakai orang banyak. Berbasis web dan mendukung sistem reminder SMS dengan bantuan situs ifttt.com.
* KampusGanesha.com (2012; Launched; CEO; Inactive, Discontinued)
    Merupakan portal yang berfokus pada sharing event-event dan pertukaran informasi di komunitas mahasiswa ITB. Merupakan Start-up yang berada di inisiasi dan berada di bawah supervisi Depkominfo KM-ITB
* Koeltwit.com (2013; Down; Programmer & owner; Discontinued) 
    Sebuah web-service yang menyediakan fitur untuk mempermudah dan meng-otomatisasi pembuatan kultwit untuk tujuan promosi maupun internet marketing.
* Taban Adventure (2015; http://bit.ly/tabanadventure; CTO; Inactive)
    Sebuah Start-Up di bidang pendidikan yang memiliki produk berupa sebuah mobile game edukasi untuk pembelajaran bahasa Inggris dengan target pasar anak SD usia 10-12 tahun. Start Up ini merupakan bagian dari mata kuliah “Technology Based Business” yang merupakan kerjasama antara program studi Kewirausahaan SBM ITB dengan Kibar, dan mendapatkan penghargaan “Judges Choice”.
* Flashback (2016; discontinued)
    Sebuah Startup yang berbasis di Yogyakarta yang mencoba menyediakan alternatif dari Buku Tahunan SMP/SMA tradisional dengan menyediakan mobile app yang interaktif dan menyenangkan. Saya berperan sebagai Back-End programmer yang bertugas membangun REST API server yang melayani mobile apps client yang dikembangkan terpisah. REST API server yang dibangun berbasis PHP7, YII2 framework, PostgreSQL, dan memanfaatkan cloud server OpenShift.
* Sistem Informasi Mentoring ITB (2016)
    Sebuah proyek dari tim Bidik Misi ITB untuk membangun sistem informasi mentoring terintegrasi yang harapannya dapat mengatasi masalah koordinasi antar berbagai lembaga mentoring keagamaan di ITB, termasuk mentoring yang diadakan oleh Program Beasiswa Bidik Misi, mata kuliah Agama & Etika Islam, UKM Keluarga Mahasiswa ITB, maupun beberapa komunitas-komunitas keislaman di jurusan-jurusan dan fakultas-fakultas di ITB.
* SatuPeta Pariwisata Riau (2018; http://pariwisata.riau.go.id/peta , http://edips.riau.go.id/onemap )
    Sebuah program kerjasama antara Dinas Pariwisata dan Ekonomoi Kreatif Provinsi Riau dengan NGO World Resource Institute Indonesia dan Software House TitikKoma Digital. Program ini merupakan proyek crowdsourcing data pariwisata yang bersifat publik dimana masyarakat dapat mengusulkan lokasi wisata yang sebelumnya belum terpublikasikan ke database peta pariwisata Provinsi Riau. Proyek ini melibatkan banyak sekali teknologi open source seperti software GIS Geoserver dan library per-peta-an Leaflet.js, serta Framework Laravel dan database PostgresSQL untuk antarmuka web nya. Situs ini bisa diakses di alamat .
* Undangan.mu ( 2018; http://undangan.mu ; discontinued )
    Startup berupa website untuk membuat undangan pernikahan berupa website. Pengguna bisa memilih desain, memasukkan informasi acara seperti lokasi acara dll, serta mengubah informasi yang ingin ditampilkan.Masih dalam tahap pengembangan.
* PertafloSIM ( 2016-sekarang; http://www.pertaflosim.com )
    Sebuah software simulasi perpipaan minyak yang merupakan hasil kerjasama antara Research Technology Center Pertamina dengan Research Consortium Oppinet ITB. Sofware ini merupakan software desktop untuk melakukan simulasi-simulasi terkait sifat-sifat fluida dan sejenisnya. Software ini dikembangkan dengan Python 2.7 berdasarkan framework PySide 1.2.4 . Saya di sini di tim RC-OPPINET ITB sebagai salah satu Programmer utama dan juga mengatur sisi DevOps untuk software ini.

# Services

- User Requirement Analysis
I provide basic user requirement analysis service for any web based software projects. This service including translating user's busines requirement into a detailed technical project plan inluding infrastrunture and feature requirement analysis, basic budget and resource forecast, and general project time plan.

- Full-stack web development
Using PHP with Yii or Laravel Framework, under windows or linux, using MySql, PostgreSQL, or SQLite. Frontend and Backend, design using Bootstrap & AdminLTE.

- Web Based GIS Software
Extension of full-stack web development. Utilize leaflet.js & MySQL Geom or Postgres PostGIS libraries. Could also use Geoserver if necessary.

- Basic DevOps and system administration
Setup basic devops for web or desktop projects. Utilize Bash or Batch file scripting, Git hooks, and Python scripting. I also provide basic server setup, installation, and maintanance.

- Desktop Application
Multiplatform software using Python , PySide library, and PyInstaller packager.

This page is Based on
---------------------
Profile - 100% Fully Responsive Free HTML5 Bootstrap Template

AUTHOR:
DESIGNED & DEVELOPED by FreeHTML5.co

Website: http://freehtml5.co/
